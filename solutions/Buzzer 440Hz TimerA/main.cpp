////////////////////////////////////////////////////////////
// Solution to produce a 440Hz signal on the buzzer with
// Timer_A0, when the button S1 on the Edu-MK-II is pressed.
////////////////////////////////////////////////////////////

#include "gpio_msp432.h"
#include "msp.h" // necessary for the CMSIS programming of Timer_A0

int main(void)
{
    // Create objects for the 2 GPIOs used
    gpio_msp432_pin button( PORT_PIN(5,1) );
    gpio_msp432_pin buzzer( PORT_PIN(2,7) );

    // Set the GPIO modes
    //
    // No pullup needed here because there is a external pullup
    // resistor on the Edu-Mk-II board!
    button.gpioMode( GPIO::INPUT );
    // Set the buzzer to output mode. This is important because
    // Pin P2.7 has two different functions when the SEL bits
    // of this pin are changed to P2SEL1=0, P2SEL0=1 (mode 1).
    // Only in output mode it is the TA0.4 output (Timer A0, CCR4)!
    buzzer.gpioMode( GPIO::OUTPUT );
    // Change the mode of the buzzer pin (see above)
    buzzer.setSEL(1); // same as P2SEL1 &= ~BIT7, P2SEL0 |= BIT7

    // Now we have to program Timer_A0, CCR4
    //
    // First, we configure the TA0CTL register, which controls
    // the overall Timer_A0 operation (clock, count mode etc.)
    // According to the MSP432 TRM (Technical Reference Manual),
    // the bit pattern 0000 0010 0001 0000 sets the clock source
    // to SMCLK (3 MHz default value), and up-mode (count until
    // value in TA0CCR0 is reached). This results in a binary
    // pattern of 0000 0010 0001 0000. This value could be
    // directly set with the legacy syntax TA0CTL = 0x210;
    // Here we use the new CMSIS syntax, which defines a struct
    // for the Timer_A registers and also defines macros for
    // every single bit field in the register:
    TIMER_A0->CTL = TIMER_A_CTL_SSEL__SMCLK | TIMER_A_CTL_MC__UP;
    //
    // Now we configure the Capture/Compare Control register 4,
    // which defines how the Capture/Control Block 4 is working
    // (capture/compare and the mode of the output unit, the
    // 'output mode'). Here we want a compare mode and want to
    // program the output unit to reset/set (mode 7). So the only bits
    // we have to change in this register are the OUTMOD bits.
    // This results in a binary pattern of 0000 0000 1110 0000.
    // The legacy syntax to set the value would be: TA0CCTL4 = 0x00e0;
    // Here we use the new CMSIS syntax:
    TIMER_A0->CCTL[4] = TIMER_A_CCTLN_OUTMOD_7;
    //
    // Set the TA0CCR0 register, which defines the upper bound
    // of the counter. The Timer_A0 is clocked with 3 MHz, so
    // we can calculate the upper counter value by dividing the
    // clock frequency with the desired frequency.
    TIMER_A0->CCR[0] = 3000000 / 440;
    //
    // Set the TA0CCR4 register so that we have a duty cycle of
    // 50%. This can be simply achieved by setting half of the
    // value of TA0CCR0.
    TIMER_A0->CCR[4] = 3000000 / 440 / 2;
    //
    // Now the Timer A0 is outputting a PWM signal with 440Hz on
    // pin 2.7! This is completely independent from CPU operation!
    //
    // How can we control the output on P2.7? There are several
    // ways to stop the Timer_A0 output on pin P2.7: We could
    // set the TIMER_A0->CTL register to stop-mode so that the
    // timer completely stops counting. Alternatively, we could
    // set back pin P2.7 to normal GPIO mode by calling setSEL(0).
    // Here we choose the first option:
    //
    while(true) {
        if (!button.gpioRead()) {
            // Button is pressed ->
            // set the mode control (MC) bits to up mode
            TIMER_A0->CTL |=  TIMER_A_CTL_MC__UP;
        } else {
            // Button is not pressed ->
            // clear the mode control bits (-> stop mode)
            TIMER_A0->CTL &= ~TIMER_A_CTL_MC_MASK;
        }
    }

}
