////////////////////////////////////////////////////
// Solution to write the accelerometer values to the
// backchannel UART (in m/s²)
////////////////////////////////////////////////////

#include <cstdio>
#include <cstdlib>
#include "adc14_msp432.h"
#include "uart_msp432.h"
#include "std_io.h"

#define CLEAR_SCREEN "%c[H%c[J",27,27

int main(void)
{
    // Setup the backchannel UART so we can see
    // the printf()-output!
    uart_msp432 uart; // default is backchannel UART!
    std_io::inst.redirect_stdin ( uart );
    std_io::inst.redirect_stdout( uart );

    // adc14 objects
    adc14_msp432_channel accel_x( 14 );
    adc14_msp432_channel accel_y( 13 );
    adc14_msp432_channel accel_z( 11 );

    // adc14 modes, we choose 12 bit resolution (0..4096)
    accel_x.adcMode(ADC::ADC_12_BIT);
    accel_y.adcMode(ADC::ADC_12_BIT);
    accel_z.adcMode(ADC::ADC_12_BIT);

    // Endless main loop
    while (true) {
        // Read the adc values as voltage (0 to 3.3V)
        // and scale to millivolts
        float x = 1000 * accel_x.adcReadVoltage();
        float y = 1000 * accel_y.adcReadVoltage();
        float z = 1000 * accel_z.adcReadVoltage();

        // Suptract the 1.65V offset (see data sheet)
        x -= 1650;
        y -= 1650;
        z -= 1650;

        // Multiply with g ...
        x *= 9.81f;
        y *= 9.81f;
        z *= 9.81f;

        // ... and divide with 0.66V per g (see data sheet)
        x /= 0.66f;
        y /= 0.66f;
        z /= 0.66f;

        // Heuristic offsets for my special sensor
        // (might be completely different for other sensors)
        x += 510; // in 1/1000 g
        z += 120; // in 1/1000 g

        printf(CLEAR_SCREEN);
        printf("Accelerometer values:\n");
        printf("X-Axis: %d.%03d\n", (int)x/1000, abs((int)x)%1000);
        printf("Y-Axis: %d.%03d\n", (int)y/1000, abs((int)y)%1000);
        printf("Z-Axis: %d.%03d\n", (int)z/1000, abs((int)z)%1000);
        printf("\n");

        // Short delay
        for (int i=0; i < 1000000; ++i) ;
    }
}
