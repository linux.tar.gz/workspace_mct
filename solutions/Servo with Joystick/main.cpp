////////////////////////////////////////////
// Solution to control servo with Joystick-X
////////////////////////////////////////////

#include "adc14_msp432.h"
#include "gpio_msp432.h"
#include "msp.h" // necessary for the CMSIS programming of Timer_A0

int main(void)
{
    // Setup the joystick (x). We choose a
    // resolution of 12 bit, so the raw value
    // will be in the range of 0 to 4096.
    adc14_msp432_channel joyx ( 15 );
    joyx.adcMode(ADC::ADC_12_BIT);

    // Setup the gpio to the servo (P2.5) to
    // be output and to work in mode 1 (TA0.2 output)
    gpio_msp432_pin servo( PORT_PIN(2,5) );
    servo.gpioMode(GPIO::OUTPUT);
    servo.setSEL(1); // same as P2SEL1 &= ~BIT5, P2SEL0 |= BIT5

    // Now we have to program Timer_A0, CCR2

    // First, we configure the TA0CTL register, which controls
    // the overall Timer_A0 operation (clock, count mode etc.)
    // According to the MSP432 TRM (Technical Reference Manual),
    // the bit pattern 0000 0010 0001 0000 sets the clock source
    // to SMCLK (3 MHz default value), and up-mode (count until
    // value in TA0CCR0 is reached). This results in a binary
    // pattern of 0000 0010 0001 0000. This value could be
    // directly set with the legacy syntax TA0CTL = 0x210;
    // Here we use the new CMSIS syntax, which defines a struct
    // for the Timer_A registers and also defines macros for
    // every single bit field in the register:
    TIMER_A0->CTL = TIMER_A_CTL_SSEL__SMCLK | TIMER_A_CTL_MC__UP;

    // Now we configure the Capture/Compare Control register 2,
    // which defines how the Capture/Control Block 2 is working
    // (capture/compare and the mode of the output unit, the
    // 'output mode'). Here we want a compare mode and want to
    // program the output unit to reset/set (mode 7). So the only bits
    // we have to change in this register are the OUTMOD bits.
    // This results in a binary pattern of 0000 0000 1110 0000.
    // The legacy syntax to set the value would be: TA0CCTL2 = 0x00e0;
    // Here we use the new CMSIS syntax:
    TIMER_A0->CCTL[2] = TIMER_A_CCTLN_OUTMOD_7;

    // Set the TA0CCR0 register, which defines the upper bound
    // of the counter. The Timer_A0 is clocked with 3 MHz, so
    // we can calculate the upper counter value by dividing the
    // clock frequency with the desired frequency, which is 50Hz
    // in this case (20ms period). With 3MHz timer operation, we
    // get a value of 60000, which still fits to the 16 bit counter!
    TIMER_A0->CCR[0] = 3000000 / 50; // 60000 -> still okay!

    // Now we can set the PWM pulse width with register TA0CCR2.
    // A pulse width of 1.5ms corresponds to 60000/20ms*1.5ms = 4500
    // A pulse width of 0.8ms corresponds to 60000/20ms*0.8ms = 2400
    // A pulse width of 2.2ms corresponds to 60000/20ms*2.2ms = 6600
    // So the desired servo signal can be set in TA0CCR2 with a value
    // range of 4500 +/- 2100
    // Our ADC14 operate in 12 bit resolution, returning values in the
    // range of 2048 +/- 2048
    // So the delta value of the ADC14 can be used directly for the
    // PWM pulse width. We only have to compensate the offset of the
    // joystick, so we read in the value now, assuming that the joystick
    // is still in the middle position.
    int joy_offset = joyx.adcReadRaw();

    // Endless main loop...
    while(true) {
        // Set the TA0CCR register to produce the correct PWM output.
        // We have to change the signs in the formula because our
        // servo changes the rotation direction (and we want to have
        // a 'right-turn' when we move the joystick to the right ... :)
        TIMER_A0->CCR[2] = 4500 + joy_offset - joyx.adcReadRaw();
    }
}
