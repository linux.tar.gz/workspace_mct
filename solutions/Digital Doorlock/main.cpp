//////////////////////////////////////////
// Solution to exercise 'Digital Doorlock'
//////////////////////////////////////////

#include "gpio_msp432.h"

// Simple delay method
void delay(int ms) {
    for (int i=0; i < ms*160; ++i) ;
}

gpio_msp432_pin s1       (PORT_PIN(1,1));
gpio_msp432_pin s2       (PORT_PIN(1,4));
gpio_msp432_pin led_red  (PORT_PIN(1,0));
gpio_msp432_pin led_green(PORT_PIN(2,1));

// Set up the GPIO modes
void init_gpios() {
    s1.gpioMode(GPIO::INPUT | GPIO::PULLUP);
    s2.gpioMode(GPIO::INPUT | GPIO::PULLUP);
    led_red.gpioMode(GPIO::OUTPUT);
    led_green.gpioMode(GPIO::OUTPUT);
}

// Small helper methods
bool button1() { return !s1.gpioRead(); }
bool button2() { return !s2.gpioRead(); }

void flashRedLed(int ms) {
    led_red.gpioWrite(HIGH);
    delay(ms);
    led_red.gpioWrite(LOW);
}

void flashGreenLed(int ms) {
    led_green.gpioWrite(HIGH);
    delay(ms);
    led_green.gpioWrite(LOW);
}

// Wait for press/release cycle on button2.
// Return true if press/release was performed.
// Return false if button 1 was released
// during this method.
bool waitForToggle() {
  // wait for button2 press
  while (!button2()) {
    if (!button1()) {
      return false;
    }
  }
  delay(10); // debounce
  // wait for button2 release
  while (button2()) {
    if (!button1()) {
      return false;
    }
  }
  delay(10);  // debounce
  return true;
}

// Helper class for key-code processing
class Code {
public:
    Code(int k0, int k1, int k2, int k3) {
        _key[0] = k0;
        _key[1] = k1;
        _key[2] = k2;
        _key[3] = k3;
    }
    void addDigit(int i) {
      _entry[0] = _entry[1];
      _entry[1] = _entry[2];
      _entry[2] = _entry[3];
      _entry[3] = i;
    }
    bool correct() {
      return (_entry[0] == _key[0]) &&
             (_entry[1] == _key[1]) &&
             (_entry[2] == _key[2]) &&
             (_entry[3] == _key[3]);
    }
private:
    int  _entry[4];
    int  _key[4];
};

Code c(2,3,0,4);
int  count = 0;
bool newEntry=false;

int main() {
    // Set up hardware
    init_gpios();
    // Endless main loop
    while(true) {
        count = 0;
        // Process button2 toggles while button1 is pressed
        ///////////////////////////////////////////////////
        while (button1()) {
          newEntry=true;  // A new digit begins...
          // Increment counter
          if (waitForToggle()) {
            count++;
            flashRedLed(20); // for debug ...
          }
        }

        // Store new digit and check for correct key
        ////////////////////////////////////////////
        if (newEntry) {
          c.addDigit(count);
          if (c.correct()) flashGreenLed(1000);
          newEntry=false;
        }
    }
}
