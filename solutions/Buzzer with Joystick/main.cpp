///////////////////////////////////////////////
// Solution to control buzzer with joystick (y)
///////////////////////////////////////////////

#include "adc14_msp432.h"
#include "gpio_msp432.h"

int main() {
    // Setup the ADC. We use the scan mode here,
    // because reading of the ADC value is much
    // faster in this mode (only a register is read,
    // and there is no waiting for a conversion).
    adc14_msp432::inst.adcSetupScan(ADC::ADC_12_BIT);
    adc14_msp432::inst.adcStartScan(9, 9);

    // Setup buzzer
    gpio_msp432_pin buzzer( PORT_PIN(2,7) );
    buzzer.gpioMode(GPIO::OUTPUT);

    while(true) {
        // Read in ADC value in the range of 0 to 4096
        int adc = adc14_msp432::inst.adcReadScan(9);

        // We need to produce frequencies in the range of
        // 100Hz (10000us) to 2000Hz (500us).
        // Because we have to set the buzzer high and low
        // within one period, we have to wait 250us to 5000us
        // between the toggles.
        // This fits quite well to our 0 to 4095 ADC output...
        // Change joystick direction (high to low)
        adc = 4095 - adc;
        // Add a 250us offset
        adc += 250;
        // The adc value is now in the range of 250 to 4346,
        // which is nearly the time in us we need!

        // Toggle the buzzer
        buzzer.gpioToggle();

        // One empty loop cycle is approx. 6.66us. Since we
        // only want to use integer arithmetic, we divide by 6,
        // because the code until now will also consume some time...
        adc /= 6;

        // Wait by looping the calculated amount
        for (int i=0; i < adc; ++i) ;
    }
}

